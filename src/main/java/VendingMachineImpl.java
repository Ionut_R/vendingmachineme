//A sample implementation of VendingMachine interface represents a real world Vending Machine ,
// which you see in your office, bus stand, railway station and public places.

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VendingMachineImpl implements VendingMachine {
    // private Inventory<Coin> currentCashInventory = new Inventory<Coin>();
    private Inventory<Coin> cashInventory = new Inventory<Coin>();
    private Inventory<Item> itemInventory = new Inventory<Item>();
    private double totalSales;
    private Item currentItem;
    private double currentBalance;
    private double totalDePlata;
    int qtyAskedByUser;

    public VendingMachineImpl() {
        initialize();
    }

    // initialize machine with 5 coins of each denomination
    // and 5 cans of each Item
    private void initialize() {
        for (Item i : Item.values()) {
            itemInventory.put(i, 10);
        }
    }

    // metoda apelata de MENIU GUI -> 'SELECTIE PRODUS'
    //returnare pret produs selectat de utilizator
    @Override
    public double selectItemAndGetPrice(Item item) {
        if (itemInventory.hasItem(item)) {//cantitatea > 0
            currentItem = item;
            return currentItem.getPrice();
        }
        throw new SoldOutException("Sold Out, Please buy another item");
    }

    //metoda apelata de buton GUI -> 'SELECTIE CANTITATE'
    // calcul pret total = pret produs X cantitate selectata de utilizator
    @Override
    public double calculateTotal(int qtyAskedByUser) {
        if (itemInventory.getQuantity(currentItem) >= qtyAskedByUser) {
            totalDePlata = currentItem.getPrice() * qtyAskedByUser;
            return totalDePlata;
        } else {
            throw new SoldOutException("Desired quantity is too big");
        }
    }

    // metoda apelata din GUI -> ACCEPT button
    //bani introdusi, atat ca suma cat si ca monede luate separat si introduse doar cate 1 pe rand
    //repet metoda atat timp cat isFullPaid = false
    @Override
    public void insertCoin(Coin coin) {
        if (!isFullPaid()) {
            currentBalance = currentBalance + coin.getDenomination();
            cashInventory.add(coin, 1);
        }
    }

    //produs achitat
    private boolean isFullPaid() {
        if (currentBalance >= totalDePlata) {
            return true;
        }
        return false;
    }

    //elibereaza rest
    private List<Coin> collectChange() {
        double changeAmount = currentBalance - totalDePlata;
        List<Coin> change = getChange(changeAmount);
        updateCashInventory(change);
        currentBalance = 0;
        currentItem = null;
        totalDePlata = 0;
        return change;
    }

    // elibereaza rest dc nu ai cumparat produsul, adica suma pe care ai introdus-o
    @Override
    public List<Coin> refund() {
        List<Coin> refund = getChange(currentBalance);
        updateCashInventory(refund);
        currentBalance = 0;
        currentItem = null;
        return refund;
    }

    //scadere cantitate produse din inventar si afisare rest ramas in aparat
    //in caz ca produsul este eliberat, restul banilor raman in aparat (currentBalance), disponibili pentru urmatorul ciclu

    private boolean hasSufficientChange() {
        return hasSufficientChangeForAmount(currentBalance - currentItem.getPrice());
    }

    private boolean hasSufficientChangeForAmount(double amount) {
        boolean hasChange = true;
        try {
            getChange(amount);
        } catch (NotSufficientChangeException nsce) {
            return hasChange = false;
        }
        return hasChange;
    }

    private void updateCashInventory(List<Coin> change) {
        for (Coin c : change) {
            cashInventory.deduct(c, 1);
        }
    }

    @Override
    public Bucket<Item, List<Coin>> collectItemAndChange() {
        Item item = collectItem();
        totalSales = totalSales + currentItem.getPrice();
        List<Coin> change = collectChange();
        return new Bucket<Item, List<Coin>>(item, change);
    }

    private Item collectItem() throws NotSufficientChangeException, NotFullPaidException {
        if (isFullPaid()) {
            if (hasSufficientChange()) {
                itemInventory.deduct(currentItem, qtyAskedByUser);
                return currentItem;
            }
            throw new NotSufficientChangeException("Not Sufficient change in Inventory");
        }
        double remainingBalance = currentItem.getPrice() - currentBalance;
        throw new NotFullPaidException("Price not full paid, remaining : ", remainingBalance);
    }

    private List<Coin> getChange(double amount) throws NotSufficientChangeException {
        List<Coin> changes = Collections.EMPTY_LIST;

        if (amount > 0) {
            changes = new ArrayList<Coin>();
            double balance = amount;
            while (balance > 0) {
                if (balance >= Coin.CINCIZECI.getDenomination()
                        && cashInventory.hasItem(Coin.CINCIZECI)) {
                    changes.add(Coin.CINCIZECI);
                    balance = balance - Coin.CINCIZECI.getDenomination();
                    continue;

                } else if (balance >= Coin.DOUAZECI.getDenomination()
                        && cashInventory.hasItem(Coin.DOUAZECI)) {
                    changes.add(Coin.DOUAZECI);
                    balance = balance - Coin.DOUAZECI.getDenomination();
                    continue;

                } else if (balance >= Coin.ZECE.getDenomination()
                        && cashInventory.hasItem(Coin.ZECE)) {
                    changes.add(Coin.ZECE);
                    balance = balance - Coin.ZECE.getDenomination();
                    continue;

                } else if (balance >= Coin.CINCI.getDenomination()
                        && cashInventory.hasItem(Coin.CINCI)) {
                    changes.add(Coin.CINCI);
                    balance = balance - Coin.CINCI.getDenomination();
                    continue;

                } else if (balance >= Coin.UNU.getDenomination()
                        && cashInventory.hasItem(Coin.UNU)) {
                    changes.add(Coin.UNU);
                    balance = balance - Coin.UNU.getDenomination();
                    continue;

                } else {
                    throw new NotSufficientChangeException("NotSufficientChange, Please try another product");
                }
            }
        }
        return changes;
    }

    //metoda apelata de DECLINE button
    // reset aparat
    @Override
    public void reset() {
        cashInventory.clear();
        itemInventory.clear();
        totalSales = 0;
        currentItem = null;
        currentBalance = 0;
    }
}

        /*public void printStats () {
            System.out.println("Total Sales : " + totalSales);
            System.out.println("Current Item Inventory : " + itemInventory);
            System.out.println("Current Cash Inventory : " + cashInventory);
        }*/

       /* private void updateCashInventory (List < Coin > change) {
            for (Coin c : change) {
                cashInventory.deduct(c);
            }
        }*/

        /*public long getTotalSales () {
            return totalSales;
        }
    }*/


