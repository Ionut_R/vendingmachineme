
//An Exception, thrown by Vending Machine when a user tries to collect an item, without paying the full amount.

public class NotFullPaidException extends RuntimeException {
    private String message;
    private double remaining;

    public NotFullPaidException(String message, double remaining) {
        this.message = message;
        this.remaining = remaining;
    }

    public double getRemaining() {
        return remaining;
    }

    @Override
    public String getMessage() {
        return message + remaining;
    }
}
