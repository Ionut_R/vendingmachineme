//A Java class to represent an Inventory, used for creating case and item inventory inside Vending Machine.
//An Adapter over Map to create Inventory to hold cash and Items inside Vending Machine

import java.util.HashMap;
import java.util.Map;

public class Inventory<T> {
    private Map<T, Integer> inventory = new HashMap<T, Integer>();

    public int getQuantity(T item) {
        Integer value = inventory.get(item);
        return value == null ? 0 : value;
    }

    public void add(T item, int qtyAskedByUser) {
        int count = inventory.get(item);
        inventory.put(item, count + qtyAskedByUser);
    }

    public void deduct(T item, int qtyAskedByUser) {
        if (hasItem(item)) {
            int count = inventory.get(item);
            inventory.put(item, count - qtyAskedByUser);
        }
    }

    public boolean hasItem(T item) {
        return getQuantity(item) > 0;
    }

    public void clear() {
        inventory.clear();
    }

    public void put(T item, int quantity) {
        inventory.put(item, quantity);
    }
}
