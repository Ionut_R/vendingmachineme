//Java Enum to represent Coins supported by Vending Machine


public enum Coin {
    CINCI(0.05), ZECE(0.10), DOUAZECI(0.20), CINCIZECI(0.50), UNU(1);
    private double denomination;

    private Coin(double denomination) {
        this.denomination = denomination;
    }

    public double getDenomination() {
        return denomination;
    }
}

