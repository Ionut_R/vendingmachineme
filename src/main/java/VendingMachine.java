//The public API of vending machine, usually all high-level functionality should go in this class

import java.util.List;

public interface VendingMachine {

    public double selectItemAndGetPrice(Item item);

    public double calculateTotal(int qtyAskedByUser);

    public void insertCoin(Coin coin);

    public List<Coin> refund();

    public Bucket<Item, List<Coin>> collectItemAndChange();

    public void reset();
}

