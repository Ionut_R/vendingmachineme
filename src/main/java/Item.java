
//Java Enum to represent Item served by Vending Machine

public enum Item {
    CRISPS("Crisps", 0.75),
    MARSBAR("Mars Bar", 0.70),
    COCACOLA("CocaCola", 1),
    EUGENIA("Eugenia", 0.5),
    WATER("Water", 0.85);
    private String name;
    private double price;

    private Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

